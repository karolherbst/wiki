[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/EN/[[ES|Nouveau_Companion_20-es]]/[[FR|Nouveau_Companion_20-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## The irregular Nouveau-Development companion


## Issue for May, 26th


### Intro

Here we are again serving you with issue number 20 of the TiNDC. 

Pq did write a introductory page about all the important topics regarding driver programming. It links to other documents and should give you a rough overview, of what to do when developing for nouveau. 

Still no decision on our introduction to the Software Freedom Conservancy. Our project was not discussed during the April meeting but we are scheduled for the evaluation meeting in May which is "soon" (obviously we should know in 5 days at most). 

No change with our infrastructure problems. Quota on SF exceeded and no other server available yet. But just send dumps in (especially newer, old, SLI or quadro cards are always welcome). Don't worry, they won't be lost. 


### Current status

Seems as if the randr12 branch is slowly getting better now. Killertux reported success on his NV43 card, however, the combined width of both screens couldn't get wider than 1280 pixel, which (for a 2 display wide screen) was a bit small. As Airlied was on vacation, there were no further changes to the branch. 

Work continues on the NV1x range of cards. Matc, pmdata and others did further tests. Currently the cards crashes when certain viewport commands are issued. Marcheu is quite certain that it did work, according to his notes and the commits to git around March, 10th. Remarks within TiNDC #15 seem to support that. As currently all cards up to NV3x crash when VIEWPORT_ORIGIN is set, a commit after that date must have broken this feature. Still matc is hunting the bug. 

Debugging (with git-bisect) however is difficult as around that date the DRM api has changed quite often. Nevertheless, careym did volunteer to try to find out the cause of breakage but had no luck yet. 

KoalaBR finally added two more scripts into the sourceforge CVS: 

* crashdump  
Collects all interesting data about a crash of the X11 Server. It is similar to the nvidia-bug-report.sh tool. Please do test and let KoalaBR know how it works (or not) on your system. 
* createdump Downloads, compiles, runs and creates a archive of renouveau tests (ready to mail) of renouveau. This is intended for novice users only. It explains every step and checks for problems.  
As promised, reverse engineering of the G84 based cards which are now in our hands started. As the design seems to be very different to what we had previously, this may take some time. 

Our findings so far seems to indicate that: 

* On card before NV50, objects are reference with their offset in RAMIN. On NV50, channels have their own hash tables,  and each object is pointed at relative to a per-channel "base" address 
* The PRAMIN PCI BAR has become paged. It doesn't just map over the end of vram in 512 Kb blocks anymore 
We have already added some new commands / objects to renouveau so that dumps on G8x cards should less often say "NV50_TCL_PRIMITIVE_3D". 

hellcatv came into our channel and let us know that by pure chance he was in a presentation which explained another effort to reverse engineer G70 and G8x based cards (clean room). The person behind it was quite far and didn't know about our project. So hellcatv asked if he may refer his email address to our project and he was quite interested. So marcheu contacted him by email. If this works out our still very limited view of the G8x may be drastically expanded. 

Well, I heard complains from a certain developer that rules-ng wasn't covered in the TiNDC very often. Although I beg to differ, here some more information about this: 

pq announced that he has started to integrate his rules-ng work back into mmio-parse. Shortly after the announcement Thunderbird requested more information as he is still searching for the register to (de-)actived tv out on Nvidia cards. Unfortunately, pq's patches at this time were not published. Until that date, Jrmuizel didn't have time to integrate them as he just got them a few hours ago. After getting his hands on the newest version, Thunderbird had some questions about how rules-ng worked, as his changes to the xml-database didn't show up in the mmio-parse. 

The current version of rules-ng uses an intermediate step which parses the xml file and creates an c-library from it. Only information compiled into this database will show up int the mmio-parses. 

Hm, I was just corrected. Obviously I did missunderstand him: he thought that progress was so miniscule that reporting on it wouldn't be worthwhile. Well, I beg to differ once again :) 

As this was the first time rules-ng was put to the test in a "real life" scenario, bugs were found and fixed, missing features (doc tags) were added and life was good. 

Finally Thunderbird got the data in the dumps he wanted and embarked on a quest to find out more information about the tv-out registers and NVidia cards. 

Work on adding G8x support for 2D operation was started by darktama. He tried to get modesetting working (for now in the DDX, but it should go into the DRM) but for some unknown reason the code failed to do what was expected of it. After some prodding though, Darktama got it working :) So you should be able to have working 2D. Beware though: After returning to text mode there is a very high probability that your display is garbled. We are working on it. 

We already pointed it out in an earlier issue: We can't just copy the nv code and get it working. We need to adjust the code to the DRM. And adding obfuscated code is not really an option... 


### Help needed

We are looking for MMio dumps of cards which currently do not run glxgears correctly. 

The aforementioned scripts from KoalaBR could need some more testing. Especially the crashdump script. Please let KoalaBR know whether it works for you or not. 

[[<<< Previous Issue|Nouveau_Companion_19]] | [[Next Issue >>>|Nouveau_Companion_21]] 
