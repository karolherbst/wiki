<meta name="google-translate-customization" content="38b387022ed0f4d4-a4eb7ef5c10c8ae0-g2870fab75904ce51-18"></meta>
<div id="google_translate_element"></div>
<script type="text/javascript" src="translate.js"></script>

## Marcin Kościelnicki

Hardware: 

* NV01: Diamond Edge 3D 
* NV03: Riva128 
* NV03Z: Riva128 ZX 
* NV04: RIVA TNT 
* NV05: RIVA TNT2 Model 64/Model 64 Pro 
* NV05: RIVA TNT2 Model 64/Model 64 Pro 
* NV05: RIVA TNT2 Model 64/Model 64 Pro 
* NV05: RIVA TNT2 Model 64/Model 64 Pro 
* NV10: GeForce 256 Ultra 
* NV11: GeForce2 MX/MX 400 
* NV11: GeForce2 MX/MX 400 
* NV15: GeForce2 GTS/Pro 
* NV17: GeForce4 MX 420 
* NV18: GeForce4 MX 440 AGP 8x 
* NV1A: nForce IGP 
* NV1F: nForce 2 IGP 
* NV20: GeForce3 Ti 200 
* NV28: GeForce4 Ti 4200 AGP 8x 
* NV30: GeForce FX 5800 
* NV31: GeForce FX 5600 
* NV34: GeForce FX 5200 
* NV35: GeForce FX 5950 Ultra 
* NV36: GeForce FX 5700 
* NV40: GeForce 6800 GT 
* NV42: GeForce 6800 GS 
* NV43: GeForce 6600 
* NV44: GeForce 6200 TC 
* NV46: GeForce 7300 LE 
* NV47: GeForce 7800 GTX 
* 2xNV49: GeForce 7950 GX2 
* NV4A: GeForce 6200 [AGP] 
* NV4B: GeForce 7600 GS 
* NV4E: GeForce 6100 
* NV63: GeForce 7050 / nForce 610i 
* NV50: GeForce 8800 GTX 
* NV50: GeForce 8800 GTX
* NV84: GeForce 8600 GT
* NV86: GeForce 8400 GS 
* NV98: GeForce G100 
* NVA0: GeForce GTX 260 
* NVA5: GeForce GT220 
* NVAC: ION 
* NVAF: GeForce 320M 
* NVC0: GeForce GTX 470 
* NVC1: GeForce GT 430 
* NVC8: GeForce GTX 570 Rev. 2
* NVCE: GeForce GTX 560 Ti
* NVCF: GeForce GTX 550 Ti 
* NVD9: GeForce GT 520 
* NVD7: GF117M
* NVE4: GeForce GTX 680 
* NVF1: GeForce GTX 780 Ti
* NV108: GeForce GT 640

Email: [[koriakin@0x04.net]]   
 IRC nick: mwk 

Working on: 

* REing random stuff all over the card. 
* Maintaining [[envytools|https://github.com/envytools/envytools]], repository of nvidia GPU documentation and RE tools 
