[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_27]]/[[ES|Nouveau_Companion_27-es]]/[[FR|Nouveau_Companion_27-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau (TiNDC)


## Édition du 28 septembre 2007


### Introduction

Ok, je suis de retour de vancances et je vais essayer de résumer ces quatre dernières semaines. 

Mais avant cela, commençons par quelques commentaires sur les retours que j'ai par rapport à la publication sur Phoronix. Pour faire court : nous avons fait naître le soupçon que nous allions nous vendre. 

Je suis vraiment flatté, je n'aurais jamais imaginé avoir des lecteurs tellement passionnés par rapport au TiNDC et où il est publié d'abord. 

Donc pourquoi ce changement ? À la base pour avoir la possibilité de toucher une audience plus large, et peut-être y récupérer quelques contributeurs. Nous sommes pragmatiques : tout ce qui pourrait aider le projet à avancer vaut le coup d'essayer et nous le faisons moins pour nous que pour vous. Pour nous, ne pas essayer c'est perdre, ou abandonner. 

Avant que nous jugions de l'utilité de nos actions, pourquoi ne pas leur laisser une chance ? Pendant les premiers 3 jours nous avons eu 14000 visites provenant de phoronix.com et cela avant même que l'article soit mentionné sur Linuxgames.com et digg. Il reste encore à voir si cela fait venir de nouveaux contributeurs. 

Les prochaines éditions n'auront pas un délai aussi long entre la publication sur phoronix et sur le wiki, mais à cause des fuseaux horaires il y aura un délai d'environ un jour. 

Eh bien, depuis notre dernier TiNDC nous avons vu AMD libérer leurs spécifications à la communauté Open Source. Nous sommes heureux que nos collègues du projet radeon soient aussi chanceux, avec un peu de chance NVidia verra également la lumière un de ces jours. 


### Statut actuel

La fin du mois d'août a vu un autre déplacement d'une partie de nos services depuis des comptes personnels (à l'école, université, etc.) vers fd.o. La page de statut de JussiP et le log de Marcheu ont été déplacés sur fd.o. Et le script de Kmeyer pour mettre à jour le dossier des dumps vers gmail a été adapté en conséquence. 

Avec le travail d'ahuillet sur Xv en ce moment, le centre d'intérêt des développeurs s'est déplacé sur d'autres fonctionnalités. 

Tout d'abord, stillunknown a porté le code de détection de nouveau pour libpciaccess. Cette bibliothèque est la nouvelle méthode dans Xorg 7.3 (xserver 1.4) pour gérer les accès au matériel d'une manière indépendante de la plate-forme, réglant ainsi les plaintes principales à propos de la détection de matériel dans Xorg. Plus tard, des tests par Marcheu sur NV04 ont montré que ça marchait bien. 

Pendant la discussion originale des la planification avec marcheu et Thunderbird, marcheu a noté que nous voulions nous libérer de la liste d'identifiants PCI dans nouveau. Il voulait utiliser uniquement des registres (comme PMC_BOOT_0) pour avoir une détection de l'architecture et éviter tout ce qui cible une carte spécifique et non une architecture. Stillunknown a implémenté ce qu'il avait demandé et a présenté un patch qui marchait avec PMC_BOOT_0 (sur NV50, ce registre est à l'offset 0x88000 à la place). 

Pendant ce temps, pmdata et matc se sont lancés dans une bataille sauvage contre le _Denial of Service_ qu'est glxgears sur les cartes NV1x. Pendant les quelques dernières éditions plusieurs problèmes ont été résolus (comme une taille FIFO invalide, une gestion incorrecte des interruptions PGRAPH) ce qui permet lentement mais sûrement de faire mieux "marcher" glxgears. Mais toujours aucune sortie. 

En essayant quelques leçons OpenGL de [[NeHe|NeHe]], pmdata a remarqué que les matrices _modelview_ et _projection_ n'étaient pas gérées correctement. p0g a tenté sa chance sur l'affichage d'un simple triangle. Pour cela il a utilisé nv10_demo, a regardé un dump renouveau pour sa carte et a ajouté toutes les donnés d'initialisation au programme. [That caused some invalid state interrupts, so everything which caused an invalid state was in  turn thrown out again.] 

Finalement, un triangle fut bien affiché, mais seulement en certaines circonstances spécifiques (comme certaines positions de la fenêtre) et ce n'était toujours pas les bonnes couleurs. p0g a investigué un peu plus et a trouvé que les valeurs VIEWPORT_SCALE_[XYZ] étaient mal calculées. Il a soumis ses modifications à la démo où pmdata a repris le bébé et tenté sa chance. Après quelques corrections mineures (la résolution était bloquée en 1280x1024), il a finalement trouvé l'erreur dans son code : le masque de couleur était défini comme valeur flottante alors qu'il aurait dû être une valeur entière. À nouveau, c'est p0g qui a découvert une autre erreur : l'ordre dans lequel la couleur et les données de positions étaient envoyées à la carte était faux. 

Donc, après tout ce travail, pmdata a finalement obtenu ceci : 

[[[[!img http://nouveau.freedesktop.org/wiki/Nouveau_Companion_27?action=AttachFile&do=get&target=20070906.png]|http://nouveau.freedesktop.org/wiki/Nouveau_Companion_27?action=AttachFile&do=get&target=20070906.png]] 

À ce moment là, tout ce qui manquait c'était une configuration correcte du buffer de profondeur. Une petite modification de Mesa par p0g a donné le résultat suivant : 

[[[[!img http://nouveau.freedesktop.org/wiki/Nouveau_Companion_27?action=AttachFile&do=get&target=nv18-glxgears.png]|http://nouveau.freedesktop.org/wiki/Nouveau_Companion_27?action=AttachFile&do=get&target=nv18-glxgears.png]] 

et a fait tomber pmdata sur le bug "final" : l'initialisation de l'état du buffer de profondeur était mal configuré. Après la résolution de ce problème, pmdata a également vu glxgears marcher. Nous voulons également remercier p0g, qui a effectué un travail de soutien important, mais je n'ai pas pu trouver un sujet pour parler de lui - désolé :) 

Mais nous avons encore quelques problèmes avec le clipping, le redimensionnement de la fenêtre vous donnera d'étranges résultats. 

Après un peu de travail sur EXA sur NV3x, marcheu est passé sur les problèmes avec le drm sur NV04. Ce travail sur nv30 était pour éviter d'avoir à utiliser le driver binaire avant d'être capable d'utiliser le moteur 3D. 

La raison du passage à NV04 était que ahuillet attendait la donation d'une carte NV04 par un supporter de notre projet (Merci!). Mais puisque DRM a été cassé par la mise à jour "TTM" (réservation d'une FIFO pour DRM), un changement de contexte fonctionnel était requis. Quelques patches avaient déjà été faits par marcheu, mais perdus dans le crash de disque dur mentionné dans la dernière édition. Donc il devait les réécrire. 

Et nv04_demo est maintenant capable de dessiner des triangles sur NV04 :) 

En essayant de faire marcher le NV04, Marcheu a remarqué quelques problèmes rares et difficiles à reproduire avec Xv, ce qui a rendu ahuillet anxieux de mettre la main sur la carte NV04 envoyée par unbeliever (Merci beaucoup!). Une première analyse a conduit au soupçon que le code de blitter ne fonctionnait pas correctement sur NV04, probablement du à une limitation en taille de la partie DMA. 

Lorsque la copie par le processeur a été forcée, tout sembla marcher. C'est à dire qu'il y a toujours un scintillement, mais à cause d'un autre problème. Donc il reste quelques bugs dans l'implémentation Xv sur NV04, mais ils n'arrivent que lorsque une résolution vraiment élevée est utilisée, et sont donc considérés mineurs. 

Une dernière remarque sur Xv pour aujourd'hui : l'overlay sur NV04 a aussi été implémenté par ahuillet. 

Du côté du G80 (pas G84!), Darktama et maccam94 ont effectué quelques tests et découvert que la 2D marchait, bien que quelques petits problèmes restaient (comme des parties manquantes des fenêtres lors du rafraichissement ou le déplacement de fenêtre s'arrêtant pour des temps très courts. 

Randr 1.2 a été mergé dans la branche principale par Airlied, ce qui implique que toutes les fonctionnalités disponibles et celles qui sont en développement sont sur la branche principale. Pour activer Randr1.2 pour nouveau dans la branche principale, utilisez : Option "Randr12" "TRUE". Le test a révélé que le _blitter_ Xv était cassé, ainsi que le _dual head_. 

Bk et stillunknown ont travaillé sur randr12 et l'ont emmené dans un état plus utilisable. C'est à dire que ça devrait marcher sur plus de cartes que précédemment. 

De versions plus récentes de xserver ont fait apparaître quelques erreurs de compilation à cause d'include récemment ajoutés, etc. Mais pmdata a pris en charge leur résolution un par un. Donc nouveau devrait marcher sur xserver 1.1 aussi, mais bien sûr sans support pour randr 1.2. Ainsi, pmdata a ajouté/étendu une option de configuration "--disable-nv30exa" qui permet de construire et d'utiliser Nouveau sur xserver >= 1.1. 

Stillunknown a remarqué quelques problèmes (comme des prototypes de fonctions manquantes) et en a parlé à Darktama. Il a testé sur son matériel et a éliminé quelques warnings en même temps. 

De plus en plus de compte-rendus à propos de randr1.2 sont arrivés, la plupart disant que ça marchait presque. La chose la plus ennuyeuse c'est que sur des écrans CRT analogiques, l'affichage semble être dévalé vers la droite avec une bordure pourpre ou rose sur le côté droit. Stillunknown avance à tâtons dans le noir puisqu'il a uniquement un écran LCD (DFP) à disposition. 

Donc il a discuté avec Thunderbird et airlied à propos de ce problème et ils l'ont tous les deux aidé avec les informations qu'ils avaient. 

bk a travaillé sur le _suspend & resume_ (depuis et vers le disque), essayant de trouver les besoins de Nouveau. La situation est compliquée mais il s'avère que le noyau manque actuellement de l'infrastructure pour suivre et être capable de changer les modes. 

Après deux jours, il a réussi à le faire marcher de telle manière qu'aucun blocage ne se produise et que X trouve la plupart des importantes données de contexte de la carte. Mais X plantait toujours lors du reveil avec un blocage DMA. 

Quelques sujets rapides: 

* pq est maintenant le mainteneur officiel de MMioTrace. Il a déjà résolu plusieurs bugs d'écritude de bug et prévoit l'ajout de nouveaux paramètres du module. 
* le support de XAA a enfin été enlevé. 
* Fedora 8 est maintenant à jour par rapport à nouvea (enfin par rapport au 20/09/2007), merci à airlied. 
* Toujours grâce à airlied, la plupart des changements demandés par notre projet par rapport à TTM sont effectués. 
* Marcheu et Darktama étaient au _X.org Developer Summit_ 
* Sur PPC, les changements de contexte avec une NV17 sont cassées. Marcheu a exprimé sont intérêt dans la résolution de ce problème. Il pensait que c'était dû au déplacement des _notifiers_ depuis la VRAM vers l'espace PCI. Un rapide patch et un petit test, et ça remarchait sur PPC. 
Un dernier sujet : nous essayons vraiment de supporter toutes les cartes nvidia depuis la Riva TNT (NV04). Mais depuis les quelques dernières semaines, nous avons du réaliser que nous ne pouvions pas faire de l'EXA sur NV04. C'est principalement à cause du fait que le NV04 ne peut pas faire de textures rectangulaires, uniquement des textures carrelées, ce qui ne va pas bien avec EXA. 

Pour accélérer EXA à l'aide d'un moteur 3D, nous devrions combiner des dalles que nous n'avons pas, puisque EXA nous donne plusieurs surfaces qui ne sont pas carrelées. Le moteur 3D du NV04 cependant attend des surfaces carrelées comme source. Nous devrions carreler les surfaces à la volée, ce qui est quelque chose de complexe à faire. Pour tous les utilisateurs de NV04 : nous sommes vraiment désolés. 

En passant : les "vraies" NV04 (Nvidia TNT1) ne marchent pas avec nouveau pour le moment car il leur manque quelques méthodes qui ne sont pas implémentées logiciellement. Darktama veut implémenter les méthodes logicielles pour NV50, donc ahuillet attend qu'il soumette cela pour ajouter le support des NV04 dans Nouveau. 


### Aide requise

Testez les modes de randr 1.2 et contactez stillunknown ou bk. 

Les problèmes sur PPC sont à voir avec marcheu. 


### Merci

Le TiNDC n'est en lui-même pas seulement un résumé du travail accompli, mais aussi un remerciement à tout le travail de nos développeurs. Mais cette fois nous avons eu quelques dons de matériel pour lesquels nous voudrions vous remercier : 

Cette fois un merci à evanfraser pour nous avoir fourni quelques (vraies) quadro (NV3x). Une de celles-ci est déjà entre les mains de Darktama, et une autre avait déjà été "acquise" par notre leader. 

Et merci pour le ravitaillement de ahuillet en NV04, NV05, NV11 et NV15. Merci à vous, unbeliever et andreasn. 

Tous les développeurs sont déjà au travail et essayent d'ajouter le support de ces cartes :) 

[[<<< Édition précédente|Nouveau_Companion_26-fr]] | [[Édition suivante >>>|Nouveau_Companion_28-fr]] 
