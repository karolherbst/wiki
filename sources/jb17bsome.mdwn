<meta name="google-translate-customization" content="38b387022ed0f4d4-a4eb7ef5c10c8ae0-g2870fab75904ce51-18"></meta>
<div id="google_translate_element"></div>
<script type="text/javascript" src="translate.js"></script>


## jb17bsome

Email: jb17bsome AT SPAMFREE gmail DOT com 

[Update April 8, 2010] - I finally got my nv40 video decoder work into a better state.  I now call it vpe since I saw it in some marketing doc and it sounded cool.  Vpe is basically the dxva accel layer under that other os.  It only accelerates mpeg2.  Mpeg4 or anything higher is not accelerated at all no matter what the marketing docs say.  I say this because I have a nv49 and it barely helped for h.264 content under that other os.  For mpeg4/h.264/wmv acceleration, you need a nv50 card with its hw video decoder.  Some day I plan on looking at the nv50 hw video decoder some day since I have a nvA6. 

I created a nvfx-vpe branch under the main nouveau kernel tree: 

[[http://cgit.freedesktop.org/nouveau/linux-2.6/log/?h=nvfx-vpe|http://cgit.freedesktop.org/nouveau/linux-2.6/log/?h=nvfx-vpe]] - This branch contains all my kernel vpe changes I have done thus far.   

I did not create a drm tree/branch but the necessary patch is under here: 

[[http://people.freedesktop.org/~jb17bsome/vpe/drm/drm_nouveau_vpe.patch|http://people.freedesktop.org/~jb17bsome/vpe/drm/drm_nouveau_vpe.patch]] - A commit.txt is there for some information about it. 

I created a simple vpe demo program that tests the drm+kernel vpe layers: 

[[http://people.freedesktop.org/~jb17bsome/vpe/nv_vpe_demo/|http://people.freedesktop.org/~jb17bsome/vpe/nv_vpe_demo/]] 

I am also working on the necessary mesa changes.   I am working locally on the pipe-video branch: [[http://cgit.freedesktop.org/mesa/mesa/log/?h=pipe-video|http://cgit.freedesktop.org/mesa/mesa/log/?h=pipe-video]] I plan on merging my changes up somehow.  Well, that is my goal. 

Cards:NV49 (7900GS), NV4B(7600GT), NV4A (6200), NV4E (6150), NVA6 (G210) 

[Deleted old XVMC work.  Really, it is so confusing so best to remove it.  If someone wants it they can always find it.] 
