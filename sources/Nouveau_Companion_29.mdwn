[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/[[EN|Nouveau_Companion_29]]/[[ES|Nouveau_Companion_29-es]]/[[FR|Nouveau_Companion_29-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## The irregular Nouveau-Development companion


## Issue for October 28th


### Intro

Here we go again: Prepare for issue number 29 of the TiNDC. This version is slightly updated to the version on Phoronix, as I forgot one importan fact. 

When we were talking about our project's infrastructure in the last issues, it was mostly negative news (no scripts working, need to move to fd.o etc.). Finally, we can report something postive about it:  We have HTML versions with links and colored nicks thanks to a script by MJules. That version was well received and even allowed tagging so that you could link to a certain line in the log (by entering '==== (topic)').  You can see which topics were tagged here: [[http://people.freedesktop.org/~ahuillet/irclogs/tags_index.htm|http://people.freedesktop.org/~ahuillet/irclogs/tags_index.htm]] 

But the script gave some false positives and thus pachi went out and improved on it. The script is now able to add HTML anchors automatically  based on time stamps. You can find both script, style sheet and the coloured logs at [[http://people.freedesktop.org/~ahuillet/irclogs|http://people.freedesktop.org/~ahuillet/irclogs]] .  

This option should come in handy for the TiNDC. Let us know, whether you like this or not. 

Additionally, we have now a page where important test requests are announced. So if you want to help us out and don't want to code, please have a look here: [[http://nouveau.freedesktop.org/wiki/TestersWanted|http://nouveau.freedesktop.org/wiki/TestersWanted]] 

One last topic before we start with the progress report itself. The question came up, why there is such a long delay between phoronix.com publishing the TiNDC and me putting it up on the Wiki. It is simple: 

* A short grace period of a few hours 
* Different timezones (which fits well with reason 1) 
* And this time due to real life concerns, I wasn't able to get to my computer before Saturday noon (CEST). At that time KMeyer already was done. 

### The Current status

ahuillet added a patch to increase the performance on NV04.  The patch cached calls to BLIT_OPERATION and RECT_OPERATION. As these are software methods, calling them too often slows down the  system dramatically. ([[http://gitweb.freedesktop.org/?p=nouveau/xf86-video-nouveau.git;a=commit;h=6d8caf5e0dd915809152c52c2c56a39d76e2ed8c|http://gitweb.freedesktop.org/?p=nouveau/xf86-video-nouveau.git;a=commit;h=6d8caf5e0dd915809152c52c2c56a39d76e2ed8c]]) 

Unfortunately, later testing revealed that this patch broke  2D rendering if a SDL application switched display modes. 

At the end of the last issue we saw ahuillet, pmdata and p0g fighting the perils of texturing and coloring triangles and quads on NV1x. The result varied wildly (from changing colours over black squads to only partly of rendered texture  (1 Texel)), just pick the failure of your choice and our developer can tell you that they already suffered through it. 

But finally he had success, it was a missing bit for texturing in the init code. 

P0g did more work on NV1x blending / transparency. First tries yielded black borders around icons instead of the background. He did some investigations and found the culprit: Transparency wasn't enabled, so he whipped up a patch  [[!img http://peter.whoei.org/nouveau/icons-semi-working.png] 

(IRC: [[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-10-23#0022|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-10-23#0022]]): [[http://cgit.freedesktop.org/nouveau/xf86-video-nouveau/commit/?id=cf053adacabaee887ecedaa9967b07b3185095b5|http://cgit.freedesktop.org/nouveau/xf86-video-nouveau/commit/?id=cf053adacabaee887ecedaa9967b07b3185095b5]] 

Another problem fixed was non power of two textures (textures with a  width / height which is not a power of two)  need to have an even size. Odd widths  get rounded up. 

Although those results are essential to get EXA working on NV1x (and rumor  has it that texture may be put to good use later on in 3D) there were still some important parts missing: PictOps and masking. 

Just before our editorial deadline, ahuillet got masking and some of the PictOps done. As NV1x has no shaders, he put register combiners to good use (see insert) However, the most important PictOp A8 + A8 (Alpha 8 bit) was not yet implemented. Reason is that NV1x doesn't support A8 as a destination. Until that is implemented, EXA on NV1x  will not really feel accelerated.  [[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-10-25.htm#1545|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-10-25.htm#1545]] 

So ahuillet claimed victory over marcheu and jkolb in the race of which card would  have EXA working first: NV1x (ahuillet, p0g) or NV3x (jkolb, marcheu). 

Unfortunately, marcheu noted a few hours later that NV1x EXA can't handle XBGR which (according to his numbers) would mean that most of the  EXA calls would hit software  fallbacks. ([[http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-10-26#0247|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-10-26#0247]]) 

So a few hours later he comitted a working fix for NV3x EXA which had all known issues ironed out.  A8 op A8 was later fixed through a DRM update and is now working, and repeat of 1x1  areas is working as well. What is missing for full EXA completeness is repeat  of > 1x1 areas, but it is not widely used and would require using a shader to compute  the required coordinate wrapping. Unfortunately, real NV30 cards do not work currently as the init in the DRM seems to be incorrect (These would be GeForce 5800, Quadro FX1000). [[http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-10-26#0451|http://people.freedesktop.org/~marcheu/irclogs/nouveau-2007-10-26#0451]]) 

**Insert: Register Combiners** 

---

 As NV1x is missing shaders of any type. So in order to accomplish similar things like modern cards do with shaders, you'll use register (or texture) combiners. 

Although register combiners and texture combiners work differently in 3D operations,  for our current 2D work they can be regarded as similar (if not equal). Register combiners  are hardwired into the texturing stage of the hardware. So when you setup that part of the card, you automatically get them with the default behaviour. 

Now what is default? If you render a pixel onto the  screen it is texel modulated by  the vertex color. If you want to modulate it by something else (say the alpha  of another texture) you "just" setup the register combiners accordingly which would  implement the IN operation which in turn is needed for masking. 

---

 

After ahuillet claimed mostly working EXA, pq started testing NV10 EXA on  his NV2x cards. Soon after testing he found out that DownloadFromScreen  (abbreviated as DFS) was not working. He bisected back until the last binary api breakage and still found DFS not working. 

jkolb and marcheu did some work on NV30. Some issue which got fixed: 

* EXA positioning and size of the rectangles was sometimes off. 
* The coordinates for EXA were not calculated correctly. The were transformed to be in the range [0..1] instead of [0..width]. Fixed by jkolb. 
At that point the driver was  unable to do transparency for e.g. icons. All transparent parts were painted black. After jkolb tried his luck and was defeated by the stubborness of the card,  marcheu tried his luck and got lucky<sup>W</sup>W showed his professionalism: 

Basically NV10 is now not very far behind the NV3x. It misses the A8+A8 PictOp. 

Although darktama was quiet during the last weeks, he wasn't lazy. He did some work on a NV4x TTM and Gallium prototype (just research, that code won't be released as is). After talking with marcheu about the pros and cons of the architecture both came to the conclusion that Gallium is the way to go. 

While marcheu and darktama were coming to a decision, pmdata wanted to start coding DRI function on NV1x but was asked by Marcheu not to. Reasoning: 

* Currently we want to get EXA working, with all the progress mentioned above, that won't take much longer 
* Gallium is different from current DRI, especially for NV2x and earlier cards. Working on DRI would be wasting time.  
* Marcheu is confident that >=NV3x cards won't have much problems getting a Gallium driver to work, but earlier cards will need some trickery (Which SuperMarcheu is going to supply  :)  ) 
See: [[http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-10-19.htm#2025|http://people.freedesktop.org/~ahuillet/irclogs/nouveau-2007-10-19.htm#2025]] 

With that in mind, marcheu stopped all his other nouveau related projects and set out to add a generic infrastructure to Gallium which will allow older cards with non existant or fixed pipelines to offer acceleration. For Nouveau that would be all cards < NV30.  

But as this work is generic, it will beneficial for all cards which miss vertex and shader pipelines (e.g. older radeons). After that pmdata can finally start working on DRI. 

Stillunknown was pestered by KoalaBR until he finally gave in and offered to (try to) get NV50 / G84 mode setting working. In an act of revenge he enlisted  KoalaBR as tester. First cautious tries to patch mode setting to a working state however yielded no change from the current situation. So we still can't switch back to text mode from X11. 

Some other topics worth mentioning: 

* hkBst's documentation patch for rules-ng of Xv related registers got merged by pq 
* [[IronPeter|IronPeter]] and his work on PS3 is progressing steadily 
* Due to the problem we talked about in the previous issue, notifiers are now always created in PCI memory. 
* stillunknown added some video control registers to rules-ng for NV50. 
* pmdata is working on finding out which texture formats are supported on NV1x and he did help ahuillet and p0g with NV1x problems 
* DRM got a fix which prevents the card from hanging in case of an error. 
* careym did fix a few OpenGL bugs in renouveau 
* chowmeined did fix some modesettings on NV5x which resulted pink lines on the right side and at the bottom. 
[[MmioTracing|MmioTrace]] on nv50 cards may freeze the machine hard. Discovered by KoalaBR (but unable to reproduce it later), PQ can reproduce it and tries to investigate, but he would be happy to have someone with NV50 hardware who can trigger the crash and has a genuine serial port with a serial terminal (another computer) to go with it. The task does not require much programming knowledge, but it does require the skill to configure and install a custom kernel, applying patches and tolerating lots of hard crashes and reboots once in a while. This may be a long fight, so this person should be able to stay aboard for a few weeks minimum.  


### Help needed

stillunknown is looking for 7xx0 card owners who would be willing to run a few tests for him. Furthermore he is looking for people with dual head  (whatever combination of VGA /DVI) who are willing to test randr1.2 / mode setting. 

Additionally, please have a look at the "Testers wanted" page for requirements coming up between our issues. [[http://nouveau.freedesktop.org/wiki/TestersWanted|http://nouveau.freedesktop.org/wiki/TestersWanted]] 

[[<<< Previous Issue|Nouveau_Companion_28]] | [[Next Issue >>>|Nouveau_Companion_30]] 
