

## Informe irregular sobre el Desarrollo de Nouveau


## Edición del 29 de junio


### Introducción

Este es el número 22 del cuidadosamente elaborado informe irregular sobre el desarrollo de Nouveau (TiNDC). Contiene todo el saber acumulado por el proyecto nouveau (incluyendo problemas y avances) y, probablemente como siempre, Darktama solicitará algunas correcciones incluso antes de que anuncie su aparición (¡vamos, admítelo, usas un script que detecta las nuevas versiones :)!). 

Es bueno ser precavido. Hay una razón para llamar a esto el Informe /irregular/ sobre el desarrollo de Nouveau. Dado que durante estas semanas no ha habido demasiada actividad se pospuso este número. 

Marcheu estaba desaparecido en acción (dijo algo sobre "renovar la casa") y a Darktama se le vió haciendo pruebas beta de Wine con steam y Tomb Raider Anniversary. 


### Estado actual

Aún así, conseguimos hacer algunas cosas. 

Bien, rules-ng y MMioTrace parecen formar una buena cadena de herramientas para la ingeniería inversa. Poco después de que nuestro último número se publicase, Philip Ezolt, del proyecto del controlador radeon, se pasó por nuestro canal y ofreció algunos añadidos para el subproyecto rules-ng. Preguntó si habría alguna oposición a la inclusión de datos específicos de radeon a los archivos xml de rules-ng. El objetivo sería hacer que MMIO-parse proporcionase también resultados útiles en los volcados para radeon. Naturalmente aceptamos y ahora trabaja diligentemente en sus añadidos ([[http://nouveau.cvs.sourceforge.net/nouveau/rules-ng/databases/|http://nouveau.cvs.sourceforge.net/nouveau/rules-ng/databases/]]). De paso, este es un buen momento para meter el blog del TiRDC, de la comunidad radeon ([[http://tirdc.livejournal.com/|http://tirdc.livejournal.com/]]). Tras 3 números como los nuestros, cambiaron a un sistema de publicación mediante un blog. Afirman que obtuvieron su inspiración de nuesto infrome. En cambio, nosotros recibimos nuestra inspiración por las Noticias Semanales de Wine y los resúmenes del kernel de LWN.net. 

Parece que nuestra idea de los informes irregulares está teniendo éxito (aparte del de radeon hemos visto al menos uno para la herramienta Dtrace y para asuntos sobre XML). 

Cuando pq empezó a comparar los nombres reales de algunos registros y constantes con los que se generarían con rules-ng y sus herramientas, se encontró con algunos problemas. Tras discutir el asunto con Marcheu, resultó evidente que sólo se deberían cambiar los nombres si no hay otra alternativa. Eso es para evitar problemas de referencias cuando algunas fuentes usan FOO y otras usan FOOBAR. Este problema resulta especialmente evidente al echar un vistazo a las fuentes del controlador Haiku para NVidia. Es muy confuso. 

pq modificó algunas de las restricciones de su esquema de nomenclatura y la vida volvió a ser hermosa. 

Jwstolk se distrajo algo respecto a su proyecto para la detección de macros. Se dió cuenta de que tenemos varios tipos de volcados MMIO (32 y 64 bits, y una versión antigua sin marcas de tiempo y una nueva con ellas, además de que las trazas pueden ser binarias o en texto, al pasarlas por mmio-parse), por lo que empezó a trabajar en la conversión entre las distintas versiones. Se puede encontrar el resultado de su trabajo en la siguiente dirección ([[http://jwstolk.xs4all.nl/nouveau/code/mmio-convert.c|http://jwstolk.xs4all.nl/nouveau/code/mmio-convert.c]]). 

En sus propias palabras: 

Mmio-convert realiza el trabajo inverso a mmio-parse, además de convertir entre trazas binarias en 32-bit y 64-bit, además del (próximo) formato binario, y un formato de texto sencillo. 

Pmdata está haciendo un lavado de cara de renouveau. Quiere deshacerse de la inmensa estructura de datos interna nv_object[] y obtenerla de una base de datos XML. Además, la herramienta se dividirá en una parte de volcado y otra de interpretación. Esto nos permitirá el envío de volcados más compactos y poder reinterpretarlos en cuanto se encuentren nuevas funcionalidades. Hoy tenemos que volver a pedir a las/os usuarias/os que hagan volcados. 

Darktama volvió a la acción añadiendo algunos parches (de limpieza) a la lista de correo, relacionados con la inicialización de las tarjetas NV49 y NV4b. Los parches provenían de jb17some y stephan_2303 y fueron arreglados por Darktama. Naturalmente, añadió agunas correcciones de errores en el proceso. 

Además, añadió todos los cambios de su rama de desarrollo a la rama principal. Puesto que los próximos cambios van a cambiar la API, Darktama solicita que se prueben los cambios introducidos hasta ahora. Por favor, prueba con tu hardware e infórmale sobre si funciona o no (indica el tipo de hardware y añade algunos detalles). Actualmente NV4x funciona, pero Pmdata volvió a tener bloqueos (pero únicamente al intentar ejecutar glxgears por segunda vez), y confirmó el fallo en NV1x. 

Como ya comentamos, el próximo paso implica romper de nuevo la API entre DRM y DDX. Por lo que sería conveniente asegurarse de que los últimos cambios no introducen nuevos fallos. 

No ha habido más avances en la parte del proyecto relacionada con las G8x, ya que Darktama estaba limpiando su pila de parches para las partes genéricas del driver. KoalaBR se puso en contacto con Matthew Garrett (que escribió libx86 / emux86) para preguntar sobre la vbetool y le pidió que compilase una nueva versión del servidor XOrg con la librería activada (XOrg tiene su propia versión incluída), incluso si la versión de Xorg en x86 normalmente no la necesitaría. Esto es necesario para comparar el comportamiento de Xorg con el que observamos. Por ejemplo, el problema de "la pantalla va a entrar en modo suspendido" es el mismo en la tarjeta G84 de Darktama, independientemente de la herramienta que se use. KoalaBR todavía no ha reunido el suficiente valor para acometer la tarea. 

Bien, ahuillet está preparando su proyecto de Xv. Lo primero que hizo fue escribir un resumen de las cosas que pretende hacer a lo largo de su proyecto. Se puede ver en [[http://nouveau.freedesktop.org/wiki/ArthurHuillet|http://nouveau.freedesktop.org/wiki/ArthurHuillet]]. Lo añadió al pie de la página con su documentación. 

Después de obtener el asentimiento de Darktama, preparó su repositorio de git en fd.o aunque se encontró con algunos problemas para comprender los conceptos de git. Resultó especialmente confuso al fijar el uso de nuestro repositorio principal de DDX como fuente original mientras que sus cambios se realizarían en el repositorio local. 

Tras resolver estos problemas, empezó con un parche sencillo que cambiaba la estructura  de [[XvPutImage|http://nouveau.freedesktop.org/wiki/XvPutImage]] para que sea posible añadir fácilmente transferencias DMA más adelante. El próximo paso fue la escritura de un parche para AGP para permitir la copia DMA de la apertura a la ram de video. 

Es posible hacer de forma sencilla el DMA con AGP, porque AGP se diseñó expresamente para ello: proporciona una GART (básicamente una tabla de relocalización) que permite que la tarjeta disponga de acceso a una gran cantidad de la memoria del sistema, que se muestra como contígua a la memoria de la tarjeta. Esta funcionalidad hace muy sencillo el uso de DMA : Básicamente copias datos a la apertura y luego se realiza el DMA apuntando la tarjeta a ella e indicando el tamaño de los datos que han de ser copiados. PCI-e debería funcionar de manera parecida, pero necesita más pruebas. Pmdata y cmn se ofrecieron para probar el parche, y los resultados de CMN en una cpu AMD cpu a 350Mhz indican problemas de time out tras 1 minuto. Sin embargo, nv y nouveau sin parches también presentan el mismo problema en su caso. 

Darktama vino al rescate y le pidió que comentase la llamada a NVWaitVSync(). CMN comentó que todo funcionaba bien así y Darktama dijo que era obviamente fallo suyo. El código ahora inicializa un contexto PGRAPH "en blanco" que limpia algunos bits del registro NV10_PGRAPH_SURFACE. Los valores usados son "mágicos", en el sentido de que se han obtenido a partir de nuestras herramientas de ingeniería inversa y no los comprendemos totalmente (o, peor, en absoluto). Una solución adecuada al problema está en camino. 

Al comentar detalladamente su parche a marcheu, se inició una discusión sobre copia de objetos de memoria y DMA, de la que entendí lo siguiente: - Las tarjetas NVidia ovrecen un objeto de memoria PCI y otro de memoria AGP. Además tienen su propia memoria frame buffer). El objeto PCI y AGP difieren en un aspecto principal: el objeto PCI apunta a una lista de páginas asignadas, mientras que el AGP apunta a una dirección y tiene un tamaño. 

Ambos objetos son necesarios para las transferencias DMA pero, por favor, sigue leyendo. - el DRM asigna la memoria AGP, PCI (todavía no, como se explica más abajo) o FB y devuelve una dirección física. - la tarjeta espera obtener un desplazamiento sobre el objeto de memoria actual, donde cada uno de estos objetos se refiere a una zona de memoria, bien AGP, PCI  o FB. En nuestro sistema tenemos un objeto AGP (llamado NvDmaAGP) y uno FB (llamado NvDmaFB). Ambos cubren todo el rango de memoria disponible para ellos (para FB, la memoria de la tarjeta, y para AGP, la memoria de la apertura). - Entonces, si quieres iniciar una copia de memoria a memoria, se toma el objeto NVidia Memory to Memory y se conectan a él los dos objetos de memoria como fuente y destino. 

Esa es la teoría, pero, desafortunadamente, todavía no podemos crear objetos de memoria PCI. Por otro lado, rivatv ya ha hecho exactamente eso. Por lo que únicamente debemos ver cómo lo han hecho y reescribirlo para trabajar en nuestra controladora. 

Marcheu arregló la doble llamada a la inicialización de la matriz de proyección pero indicó que todavía tenía problemas con la swtcl (transformación, recorte e iluminación por software). Debido a compromisos en la vida real, se limitó a contestar las preguntas. 

Sin embargo, el problema con el viewport mencionado en el último número todavía persiste. 

Finalmente, kmeyer obtuvo una cuenta en fd.o y creó un nuevo directorio de volcados. Él y JussiP están ahora el proceso para restablecer la página de estado ([[http://users.tkk.fi/~jpakkane/ren/|http://users.tkk.fi/~jpakkane/ren/]]) . 


### Ayuda solicitada

Por favor, comprobad nuestro DRM y DDX actual e informad sobre vuestro éxito o fallo. Necesitamos saber que todo sigue funcionando igual que antes, ya que el próximo paso romperá la API interna, por lo que será más complicado rastrear más adelante los problemas introducidos. 

Si no os importa, probad también los parches de ahuillet en git://people.freedesktop.org/~ahuillet/xf86-video-nouveau e informadle de cómo os funcionan. Sin embargo, estad listos para encontraros con problemas, disfuncionalidades y bloqueos, ya que ¡todavía es un trabajo en marcha! 
