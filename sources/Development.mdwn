<meta name="google-translate-customization" content="38b387022ed0f4d4-a4eb7ef5c10c8ae0-g2870fab75904ce51-18"></meta>
<div id="google_translate_element"></div>
<script type="text/javascript" src="translate.js"></script>

<a name="Tools"></a> 
### Tools

Tools that help the driver writing process: 

* [[EnvyTools|http://github.com/envytools/envytools]] - hardware register database, documentation and collection of RE tools (nvapeek, nvapoke, nvbios, envydis, etc.) 
* [[MmioTrace]] - traces memory mapped I/O access within kernel. Used to record how a driver module uses MMIO reads and writes. 
* [[Valgrind-mmt]] - modification of [[Valgrind|http://valgrind.org/]] which intercepts all traffic to the driver 
* [[Nouveau-PM|http://nouveau.git.sourceforge.net/git/gitweb.cgi?p=nouveau/nouveau;a=tree;f=nouveau-pm]] 
* [[REnouveau]] ([[CVS|http://nouveau.cvs.sourceforge.net/nouveau/renouveau/]]) - dumps the modifications in the command fifo and user-mapped registers when a process emits rendering commands. 

Obsolete: 

* [[Nouveau CVS repo|http://nouveau.cvs.sourceforge.net/nouveau/]] - other tools and scripts (e.g. crashdump and createdump). 

<a name="Docs"></a> 


#### Debugging tools

* perf is a useful profiler, for a system wide performance overview.

### Documentation/Available source code


#### Documentation about how nvidia hardware works

* [[EnvyTools|http://github.com/envytools/envytools]] - hardware register database, documentation and collection of RE  tools (nvapeek, nvapoke, nvbios, envydis, etc.) 
* [[NvHardwareDocs|NvHardwareDocs]] all documented nv registers, hardware protocols, articles, and miscellaneous hardware info about nv vid cards 

#### Introduction and API docs

* [[IntroductoryCourse]] - selected links and documents as a course material 
* [[How to write a Mesa driver|http://dri.freedesktop.org/wiki/MesaDriver]] 
* [[The DRI documentation page|http://dri.freedesktop.org/wiki/Documentation]] 
* [[A locking mechanism for the DRI|http://dri.sourceforge.net/doc/hardware_locking_low_level.html]] 
* [[Graphics Stack Overview|GraphicStackOverview]] 
* [[A simple git guide|http://wiki.sourcemage.org/Git_Guide]] 
* [[How to build and install a full graphics stack from git|http://wiki.x.org/wiki/Development/git]] without overwriting your distribution provided X. 
* [[KernelModeSetting]] 
* [[The State of Linux Graphics (2005)|https://sites.google.com/site/jonsmirl/graphics]] 
* [[Sharpening the Intel Driver Focus (2009)|http://keithp.com/blogs/Sharpening_the_Intel_Driver_Focus/]] 
* [[The Linux Graphics Stack (2012)|http://blog.mecheye.net/2012/06/the-linux-graphics-stack/]] 
* [[i915 GEM crashcourse (driver-specific GPU buffers) (2012)|http://blog.ffwll.ch/2012/10/i915gem-crashcourse.html]] 
* [Reverse Engineering Power Management on NVIDIA GPUs (2013)](http://phd.mupuf.org/publication/2013/09/25/reverse-engineering-power-management-on-nvidia-gpus/)
* [Virtualization and Migration with GPGPUs (2013)](https://os.ibds.kit.edu/97_2662.php)

#### Presentations

* [[Marcheu's talk about Xorg Myths (FOSDEM 2007)|http://icps.u-strasbg.fr/~marchesin/nvdri/fosdem1.pdf]] 
* [[Marcheu's talk about Nouveau (FOSDEM 2007)|fosdem2.pdf]] [[old link|http://icps.u-strasbg.fr/~marchesin/nvdri/fosdem2.pdf]] 
* [[The FOSDEM 2006 presentation|fosdem-nvidia-slides.sxi]] 
* [[David Airlied's LCA 2007 talk|http://www.skynet.ie/~airlied/talks/lca07/nouveau.odp]]
* [A Deeper Look Into GPUs and the Linux Graphics Stack (2012)](http://phd.mupuf.org/files/toulibre2012_deeper_look.pdf)

#### Video Hardware docs

* [[A trip through the graphcs pipeline 2011|https://fgiesen.wordpress.com/2011/07/09/a-trip-through-the-graphics-pipeline-2011-index/]] - a great and detailed explanation of the various stages of a 3d pipeline
* [[X.Org How Video Cards Work|http://wiki.x.org/wiki/Development/Documentation/HowVideoCardsWork]] 
* [[Alex Deucher's "Understanding GPUs from the ground up" blog post|http://www.botchco.com/agd5f/?p=50]] 

#### Other nVidia drivers

* [[pscnv|http://github.com/pathscale/pscnv/wiki]], a redesigned fork of nouveau for NV50+ cards, driven by [[PathScale|PathScale]]. 
* [[The DirectFB nVidia driver|http://www.directfb.org/index.php/viewcvs.cgi/DirectFB/gfxdrivers/nvidia]] **[[New address of DirectFB git repo|http://git.directfb.org/?p=core/DirectFB.git;a=summary]]** 
* [[The BeOS/Haiku nVidia driver|http://web.inter.nl.net/users/be-hold/BeOS/NVdriver/index.html]] ([[svn header|http://dev.haiku-os.org/browser/haiku/trunk/headers/private/graphics/nvidia/]]; [[svn driver|http://dev.haiku-os.org/browser/haiku/trunk/src/add-ons/accelerants/nvidia]]) 
* [[Utah-GLX|http://utah-glx.sourceforge.net/]] 
* [[The xfree 3.3.3 nvidia driver just after obfuscation|http://cvsweb.xfree86.org/cvsweb/xc/programs/Xserver/hw/xfree86/vga256/drivers/nv/?hideattic=0&only_with_tag=xf-3_3_3]] 
<a name="About"></a> 


#### Misc

* [[Graphics stack diagram|http://fs.mupuf.org/mupuf/nvidia/archi.svg]] 
* [[Mesa diagram|http://fs.mupuf.org/mupuf/nvidia/mesa.svg]] 
* [[InstallNouveau]] - guide to compiling/installing all the packages that make up nouveau
* mostly outdated [[ToDo]] 
* [[Games]] - how games use graphics 
* List of all [[CodeNames]] for NVIDIA cards 
* [[openChrome Project|http://www.openchrome.org]] - open source device drivers for VIA S3 (has updated [[XvMC]] with MPEG-4 acceleration support) 
* [[intellinuxgraphics.org|http://intellinuxgraphics.org]] open source Linux Graphics Device Drivers from Intel (with [[XvMC]] for MPEG-2 acceleration support) 
* [[R300 DRI (Direct Rendering Infrastructure)|http://dri.freedesktop.org/wiki/R300]] - open source device drivers development for AMD/ATI R300 chipset 
* [[NouveauDemo|NouveauDemo]] a test suite 
* [[DumpingVideoBios|DumpingVideoBios]] 
* [[NouveauTerms|NouveauTerms]] 
* [[DDC|DDC]] 
* [[Dithering|Dithering]] 
* [[HangDiagnosis|HangDiagnosis]] 
