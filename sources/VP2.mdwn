<meta name="google-translate-customization" content="38b387022ed0f4d4-a4eb7ef5c10c8ae0-g2870fab75904ce51-18"></meta>
<div id="google_translate_element"></div>
<script type="text/javascript" src="translate.js"></script>

VP2 is the video decoding acceleration engine inside NV84-NV96, NVA0 cards (see [[CodeNames]]). It corresponds to "VDPAU feature set A" in the NVIDIA documentation. It has partial acceleration for MPEG1/2 and VC-1, and full acceleration for H.264.

**WARNING:** The firmware supplied by NVIDIA (and used here as well) allows any user with graphics access to execute arbitrary code on the internal xtensa processors. This means that such a user would be able to read/write any system memory. If you have an IOMMU, this is somewhat mitigated in that the user would only be able to do arbitrary reads/writes to VRAM and CPU-side graphics objects.

Here are the steps to test it out under nouveau:

2. Extract firmware from the blob using [[`extract_firmware.py`|https://github.com/imirkin/re-vp2/blob/master/extract_firmware.py]] and place it in `/lib/firmware/nouveau`.
2. Get a 3.11-rc4 or later kernel, or [[nouveau/master|http://cgit.freedesktop.org/nouveau/linux-2.6/]], compile, boot.
2. Make sure you have Mesa 9.2 or later. If you don't want to overwrite your system install, you'll need a libvdpau installed to the new prefix as well, otherwise the `LD_LIBRARY_PATH` won't cause the new vdpau backend driver to load, and there's no way to override the vdpau search path at runtime.
2. Run `mplayer -vo vdpau -vc ffmpeg12vdpau,ffh264vdpau, foo.mp4`

If you're looking to use XvMC, just configure `/etc/X11/XvMCConfig` to point to the appropriate `libXvMCnouveau.so`, and you should then be able to use `mplayer -vo xvmc` with no issue.

Known Issues:

* H.264: Interlaced videos are not supported (but they're rare)
* H.264: Some videos show very occasional blocking effects
* MPEG2: VDPAU shows a lot of small artifacts. XvMC works fine.
* VC-1: Not supported, no plans to add support
* Some websites' flash video players get messed up (if you set `EnableLinuxHWVideoDecode = 1` in your `/etc/adobe/mms.cfg`)
