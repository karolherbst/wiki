<meta name="google-translate-customization" content="38b387022ed0f4d4-a4eb7ef5c10c8ae0-g2870fab75904ce51-18"></meta>
<div id="google_translate_element"></div>
<script type="text/javascript" src="translate.js"></script>

Nouveau is the X11 driver which should -- at a later stage -- offer 3D acceleration. Currently 2D support is quite useful and we are working on 3D acceleration and deobfuscating the source we inherited from the X11 nv driver. 


## Installing Nouveau from git using the latest git kernel and Debian GNU/linux unstable/experimental

* Install the required [[toolchain|http://nouveau.freedesktop.org/wiki/DebianTips]]. Alternatively you can install the [[nouveau-build-deps_1.0_all.deb|nouveau-build-deps_1.0_all.deb]] Debian package, which only contains dependencies on the required packages.

        dpkg -i nouveau-build-deps_1.0_all.deb # will complain about the dependencies
        apt-get -f install # should install them along with the package
        apt-get build-dep linux-image-2.6-amd64 # get all packages needed to compile a kernel
        apt-get install xdev-utils kernel-package checkinstall

    In case you'll want to deinstall all the packages afterwards you'll be able to:

        apt-get --auto-remove remove nouveau-build-deps

A loose compilation of commands for the advanced user. Latest version is available at [[http://www.alice-dsl.net/jkl345/Installations.html|http://www.alice-dsl.net/jkl345/Installations.html]] and look for Installation-Nouveau.txt. 

There are also precompiled Debian GNU/linux kernel packages available for the AMD64 architecture. The download and compilation takes quite a long time. 

This is the kind of recommended way to install Nouveau as of 12th september 2009. It's kind of a quick hack, but not so invasive to package management. 

    ###
    ### Build the Nouveau driver stuff with the Nouveau kernel from git as user source in /home/source/nouveau
    ### on a Debian GNU/linux unstable system
    ### Kernel images are built using package manager, DDX and DRM are quick hack.
    ###


This is a loose compilation of commands. Be aware that these are not copy-and-paste-ready as e.g. some *
might match more than they should. It is no robust script or whatsoever, but only a help in setting Nouveau
git kernel up. You may copy-and-paste command by command and EDIT it. Only then execute it. Know what you
do!

Free document license, no warranty at all. Copy and modify at will on YOUR OWN RISK!
Questions and comments are welcome to jkl345 <some sign like little monkey> A l i c e - D S L . N E T

Ready built kernels may be found at http://www.alice-dsl.net/jkl345/Installations.html

    exit 42         # Don't execute this automatically!


    sudo su - source
    pwd
    mkdir -p nouveau/Kernel
    cd nouveau/Kernel


    #
    # Nouveau kernel tree (DRM modules) holds the latest nouveau kernel driver.
    #
    git clone --depth=1 git://anongit.freedesktop.org/git/nouveau/linux-2.6
    cd linux-2.6
    cp -i /boot/config-`uname -r` ./.config
    make oldconfig  # and answer the questions, say Yes to NOUVEAU driver of course
    fakeroot make-kpkg --us --uc --initrd --append_to_version=-nouveau.git --revision=nouveau.1 kernel_image kernel_headers
    cd ..
    ls linux-*.deb
    sudo dpkg -i linux-*.deb
    sudo update-initramfs -c -k `uname -r`


    #
    # DRM git (DRM userspace libraries) holds libdrm and libdrm_nouveau.
    #
    sudo aptitude install libdrm-dev        # install library which we will manually overwrite
    sudo aptitude hold libdrm-dev           # put this library on hold so our files don't get overwritten by Debian on update
    sudo aptitude hold libdrm-nouveau1 libdrm2
    git clone --depth=1 git://anongit.freedesktop.org/git/mesa/drm
    cd drm
    ./autogen.sh
    ./configure --prefix=/usr --enable-nouveau-experimental-api
    make
    sudo checkinstall --pkgname nouveau-drm -D --install=no make install
    # Correct the version info and use comment: "Nouveau DRM user space from git for Nouveau kernel"
    cd ..
    sudo dpkg --force-overwrite -i nouveau-drm*.deb


    #
    # DDX git (2D driver) holds the accelerated Nouveau Xorg driver.
    #
    sudo aptitude install xserver-xorg-video-nouveau        # install driver which we will manually overwrite
    sudo aptitude hold xserver-xorg-video-nouveau           # put this driver on hold so our file does not get overwritten by Debian on update
    git clone --depth=1 git://anongit.freedesktop.org/git/nouveau/xf86-video-nouveau
    cd xf86-video-nouveau
    ./autogen.sh
    make
    sudo cp src/.libs/nouveau_drv.so /usr/lib/xorg/modules/drivers  # overwrite held Debian xserver-xorg-video-nouveau file
    cd ..
 
The information below is considered obsolete especially the instructions to install modules from mesa/drm-git. The Nouveau driver is now maintained in a copy of the kernel tree which is synced frequently with latest Linus' git. You may find valuable additional information below though. 


## Installing Nouveau from Debian Experimental (packages quite outdated as of 2009-09-12)

The Nouveau drivers are now available from Debian experimental. The term "experimental" here refers to the distribution of Debian (such as "stable", "testing", etc). 

Installing these drivers requires compilation to ensure that the generated "drm" module is compiled against your kernel correctly. If you would like to try them: 

1. Ensure you are using Debian sid/unstable. 
1. Add experimental sources to your /etc/apt/sources.list. 
1. Run the following commands to prepare your system to compile

        sudo apt-get update
        sudo apt-get install module-assistant
        sudo module-assistant update
 
1. Run the following command to compile and install drm:

        sudo DRM_MODULES=nouveau module-assistant auto-install drm
 
1. Now we have installed drm, we can install the Nouveau drivers themselves:

        sudo apt-get install xserver-xorg-video-nouveau
 
1. Edit your /etc/X11/xorg.conf and specify the nouveau driver in the Device section. 
1. Restart X. You will need to remove the binary nvidia kernel module. 
Editing your xorg.conf may be as simple as the replacing nvidia or nv with nouveau; nouveau won’t be chosen automatically over nv yet in Debian. 

If you find bugs related to the Debian packaging, please report the problem with the Debian bug tracker. 


## Problems

1. Make sure that neither `drm` nor `nvidia` (binary version) kernel drivers are loaded. Use

        lsmod | egrep "(drm|nvidia)"

    to find out if the modules are loaded and then unload them using:

        rmmod nvidia
        rmmod drm

    should you still have them loaded. 

Q: I can't install xserver-xorg-video-nouveau, it says something like "linux-nouveau-modules not available"   
A: Follow the "Installing Nouveau from Debian Experimental" instructions above; you must compile the drm modules which "Provide:" linux-nouveau-modules. 

Q: I can't insert the drm module, it says something with "duplicated symbol"   
A: Make sure that you haven't set CONFIG_DRM in your kernel config 

Q: Inserting nouveau.ko fails with "invalid module format"  
A: drm.ko needs to be inserted first. 

Q: Compiling xf86-video-nouveau fails with "No nouveau_drm.h found".   
A: Make sure that nouveau_drm.h lies in a path which can be found by your compiler. Best thing is to put it in the same directory as the headers of the other X11 drivers. Or add your include path to the makefile. 
